<?php
session_start();
try
{
    $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', '',
    //Ligne ci dessous, active les erreur de pdo.
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
$currentPseudo = $_POST['name'];
$_SESSION['pseudo'] = $currentPseudo;

$message = $_POST['msg'];
$pseudo = $_POST['name'];


$addmessage = $bdd->prepare('INSERT INTO chat (pseudo , message ) VALUES (? , ?)');

$addmessage->execute(array(
    $pseudo,
    $message
));
header("Location: ./vue.php");
?>
